import uvicorn
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from starlette.responses import RedirectResponse
import os
from pathlib import Path
import random

assets_directory = os.getenv("APP_ASSETS", "./default_assets")
assets_names = [a.name for a in Path(assets_directory).iterdir()]

app = FastAPI()
app.mount("/static", StaticFiles(directory="server/static"), name="static")
app.mount("/assets", StaticFiles(directory=assets_directory), name="assets")


@app.get("/")
async def redirect():
    return RedirectResponse("/static/index.html")

@app.get("/api/assets")
async def list_assets():
    return {
        "assets": random.sample(assets_names, len(assets_names))
    }
