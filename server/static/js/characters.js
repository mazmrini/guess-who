class Character {
    constructor(img, name) {
        this.img = img;
        this.name = name;
    }
}

export const listCharacters = async (baseUrl) => {
    const response = await fetch(`${baseUrl}/api/assets`);
    const assets = (await response.json())["assets"];

    return assets.map((asset) => new Character(`/assets/${asset}`, toName(asset)));
};

function toName(path) {
    const [name, _] = path.split(".");

    return name.charAt(0).toUpperCase() + name.slice(1);
}
