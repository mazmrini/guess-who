# guess-who

Simple frontend app in pure HTML/JS/CSS to play guess who with a friend.

## Setup

Leverages [bin](https://gitlab.com/mazmrini/bin) that you must first install globally:
```
pip install sbin
```

```
bin up
```

## Run
```
# will use the `default_assets` folder
bin <server|s>

inject APP_ASSETS to make the app point to another assets folder
APP_ASSETS=/my/path/to/pictures bin s
```
